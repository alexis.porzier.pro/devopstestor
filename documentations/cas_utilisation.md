|[Home](home.md)|
|:---:|

# Tests automatisés

## Cas d'utilisation

3 rôles sont identifiés, ces rôles peuvent être portés par la même personne

### Recette usine

La recette usine doit s'assurer de la bonne réalisation des tests et la bonne couverture des exigences

```plantuml
@startuml
:Recette usine: --> (Lancer les tests)
:Recette usine: --> (Visualiser rapports des tests)
:Recette usine: --> (Visualiser historique des résultats des tests)
:Recette usine: --> (Visualiser couverture des exigences)
@enduml
```

__Lancer tests__

Le lancement des tests s'effectue via une interface (Jenkins, ligne de commande, gitlab-ci, etc.)

__Visualiser les rapports des tests__

La visualisation des rapports des tests s'effectue via Jenkins.
Les rapport d'exécutions sont généré au format junit.xml

__Visualiser l'historique des résultats des tests__

La visualisation de l'historique des résultats des tests s'effectue via un Dashboard Kibana.
Les résultats des tests sont logués dans un fichier puis envoyés vers ELK via Filebeat

__Visualiser la couverture des exigences__

La visualisation de la couverture des exigences s'effectue via un Dashboard Kibana.
Les exigences sont à indiquer dans les vérifieurs python.
Lors de l'exécution d'un vérifieur, un log est émis et collecté dans ELK via Filebeat.

### Développeur

Les développeurs des formulas doivent pendant leurs dev les couvrir via des tests auto

```plantuml
@startuml
:Développeur formula: --> (Créer test case)
:Développeur formula: --> (Créer vérifieurs python)
:Développeur formula: --> (Lancer tests en local)
:Développeur formula: --> (Lancer tests sur une branche)
@enduml
```

__Créer test case__

Les tests cases sont à écrire dans le code de chaque formulas, il s'agit d'un fichier .yaml
Ils sont principalement écrits par les développeurs lors de la réalisation d'US

__Créer vérifieurs python__

Les vérifieurs Python permettent d'effectuer des vérifications scriptées une fois un produit déployé.
On peut les catégoriser :
* Les vérifieurs génériques permettent de valider des attributs communs à tous les produits (grains d'installation, supervision, structure map.jinja ..)
* Les vérifieurs produits permettent de vérifier l'état de marche d'un produit suite à son installation
* Les vérifieurs d'exigences guide produit

__Lancer tests en local__

Pour pouvoir développer des tests cases, il est nécessaire de pouvoir tester leurs exécutions en local, pour cela un système basé sur Vagrant est mis en place

__Lancer tests sur une branche__

Lors des phases de développement, le gitflow est de créer une branche par US. Les tests automatisés doivent pouvoir être lancé sur cette branche pour la valider avant de la merger dans la branche principale.

### Développeur tests automatisés

Le développeur des tests automatisé s'assure que le socle apporte les fonctionnalités souhaitées par les 2 autres rôles

```plantuml
@startuml
:Développeur test auto: --> (Ajouter scenarios de test)
:Développeur test auto: --> (Créer vérifieurs génériques)
@enduml
```

__Ajouter scenario de test__

Les tests cases fonctionnent en utilisant des briques de code appellés scénario qui sont des patterns d'interactions avec Saltstack. Il peut être nécessaire d'en mettre à disposition des développeurs.

__Créer de vérifieurs générique__

Il est parfois préférable de positionner des vérifieurs génériques au niveau du noyau des tests automatisés
