|[Home](home.md)|
|:---:|

# Configuration des tests automatisés

Les fichiers de configurations des tests automatisés sont situés dans le dossier `/config` du noyau et de ses implémentations, ils sont chargés dans un ordre précis avec une surcharge dans l'ordre ci-dessous :
1. `devopstestor/config`
2. `<couche_implementation>/config`
3.  Arguments passés en ligne de commande
4.  Configurations spécifiques à la méthode de lancement (paramètre `--other_conf_dirs`)

Des dossiers supplémentaires pour y inscrire les spécificités de lancement selon la méthode peuvent être ajoutés :
- `conf-dockerlocal`
- `conf-jenkins`
- `conf-vagrant`

## Utilisation de configuration

Dans chacun des dossiers de configurations présenté ci-dessus, plusieurs fichiers de configuration yaml peuvent être définis.
Lors de l'initialisation de l'outil, le contenu de ces fichiers est chargés en mémoire et chacun des paramètres devient accessible dans le code python via un chemin (ex `arguments:title`).

### Exemple
**Récupération de la valeur**
Le code ci-dessous permet de récupérer le contenu du paramètre `resume_machine` (activé le plus souvent via l'argument `--resume_machine`) présent dans le fichier `machine.yaml`.
```python
value_resume_machine = global_config.get('machine::resume_machine')
```
**Ajout d'un argument au lanceur**
La configuration ci-dessous permet d'ajouter un argument de ligne de commande permettant de modifier un paramètre.
La valeur par défaut de ce paramètre est directement définit dans le fichier `core.yml` en référence au paramètre `config_name` (`core::`)
```yaml
# Extrait du fichier `devopstestor/config/arguments.yml`
parameters:
  log_level:
    type: str
    help: log level (debug, warning or info)
    config_name: 'core::global_log_level'
```

## Rôles des fichiers configuration

**arguments.yml**: Permet de définir les arguments accessibles depuis l'invite de commande.
Pour chaque argument configuré, le champ `config_name` permet de surcharger le paramètre `config_name` isss des fichiers de configuration

**provisionner.yml**: Permet de définir et de configurer le provisionner choisit

**testcase.yml**: Définit les emplacements et la méthode de recherche des fichiers décrivant les cas de test

**core.yml**: Permet de définir les paramètres du noyau, fichier non surchargeable

**machine.yml**: Permet de définir le type de machine sur lequel les tests auto seront lancés

**source_manager.yml**: Permet de définir les points de montages, les méthodes d'accès et les éventuels traitements à effectuer sur avant montage sur la machine.
