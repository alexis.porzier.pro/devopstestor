|[Home](../home.md)|
|:---:|

# Lancement des tests automatisés

Le moteur des tests automatisé se lance directement en Python3.

```bash
python3 <chemin_vers_implementation_testauto> <arguments>
```
Afin de connaitre la liste des arguments utilisez l'argument d'aide `-h`
