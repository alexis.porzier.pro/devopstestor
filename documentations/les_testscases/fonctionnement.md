|[Home](home.md)|
|:---:|

# Fonctionnement général

## En bref :
- Les testscases décrivent un enchaînement de scénarios et de vérifications (les vérifieurs)
- Chaque appel à un scénario permet une action sur une machine puis des vérifieurs paramétrés se lance sur la machine pour vérifier l'action effectuée
  - Exemple les scénarios de déploiement sont un enchaînement de commandes saltstack

# Les classes
```mermaid
classDiagram

AbstractProvisionner --* AbstractMachineController: machine_controller
AbstractProvisionner:  Config global_config
AbstractProvisionner: initialise(machine_controller, machine_name)
AbstractProvisionner: get_machine_controller()
AbstractProvisionner: __init__(global_config, machine_controller, machine_name)

AbstractProvisionner <|-- MinionProvisionner
MinionProvisionner:initialise(machine_controller, machine_name)
MinionProvisionner:set_contexte(pillar, merge=True)
MinionProvisionner:runStateApply(saltenv=None, pillar={})
MinionProvisionner:setGrains(grains)
MinionProvisionner:getGrains(key)
MinionProvisionner:grains_append(key, val)
MinionProvisionner:grains_remove(key, val)

AbstractProvisionner <|-- LogstashProvisionner
LogstashProvisionner: initialise(machine_controller, machine_name)
LogstashProvisionner: test_config(path_settings, **others)
LogstashProvisionner: logstash_service_control(action)
LogstashProvisionner: mock_logstash_input(pipeline_conf_path, logstash_input_http_port)
LogstashProvisionner: mock_logstash_output(pipeline_conf_path, logstash_output_http_port)

AbstractProvisionner <|-- OracleProvisionner
OracleProvisionner: CX_Oracle driver
OracleProvisionner: initialise(machine_controller, machine_name)
OracleProvisionner: create_table(table_name, columns)
OracleProvisionner: insert_datas(table_name, datas)

AbstractMachineController: Config global_config
AbstractMachineController: String machine_name
AbstractMachineController: SourceManager source_manager
AbstractMachineController: pre_initialize(global_config, machine_configuration, source_manager)
AbstractMachineController: is_aldready_exists( machine_configuration, machine_name)
AbstractMachineController: create_machine( global_config, machine_configuration, machine_name, source_manager)
AbstractMachineController: resume_machine( global_config, machine_configuration, machine_name, source_manager)
AbstractMachineController: run_cmd( command)
AbstractMachineController: run_python( script)
AbstractMachineController: append_in_file( content, file_path)
AbstractMachineController: put_in_file( content, file_path)
AbstractMachineController: get_file_content( file_path)
AbstractMachineController: destroy_machine( global_config, machine_configuration, machine_name, source_manager)
AbstractMachineController: __init__( global_config, machine_configuration, machine_name, source_manager, machine_preset_name)
AbstractMachineController: __del__()


AbstractMachineController <|-- DockerController
DockerController: DockerSDK docker
DockerController: pre_initialize(global_config, machine_configuration, source_manager)
DockerController: create_machine(global_config, machine_configuration, machine_name, source_manager)
DockerController: is_aldready_exists(machine_configuration, machine_name)
DockerController: resume_machine(global_config, machine_configuration, machine_name, source_manager)
DockerController: put_in_file(content, file_path)
DockerController: get_file_content(file_path)
DockerController: run_cmd(command)
DockerController: destroy_machine(global_config, machine_configuration, machine_name, source_manager)

AbstractMachineController <|-- LocalController
LocalController: pre_initialize(global_config, machine_configuration, source_manager)
LocalController: create_machine(global_config, machine_configuration, machine_name, source_manager)
LocalController: destroy_machine(global_config, machine_configuration, machine_name, source_manager)
LocalController: put_in_file(content, file_path)
LocalController: get_file_content(file_path)
LocalController: run_python(script)
LocalController: run_cmd(command)

```


## Ordonnancement détaillé
```mermaid
sequenceDiagram
  participant User
  participant TestCase
  participant Scenario
  participant Provisionner
  participant Machine
  participant  contexte

  User ->> TestCase: Lancer testcase(testcase.yml)
    activate TestCase
      TestCase ->> contexte:Chargement
      TestCase ->> Machine:Initialise
      loop pour chaque scenario
        TestCase ->> Scenario: Jouer scenario
        activate Scenario
          loop pour chaque action
            Scenario ->> Provisionner: Lancer action
              activate Provisionner
                Provisionner ->> Machine: Lancer commande
                activate Machine
                  Machine -->> Provisionner: Résultats
                deactivate Machine
              deactivate Provisionner
              Provisionner -->> Scenario: Résultats
            end
            Scenario -->> TestCase: Résultats
          deactivate Scenario
          TestCase ->> Machine: Lancer verifiers
          activate Machine
            Machine -->> TestCase: Résultats
          deactivate Machine
      end
      TestCase ->> Machine:Nettoyage
      TestCase -->> User: Résultats
    deactivate TestCase
```
