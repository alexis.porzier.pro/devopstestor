|[Home](home.md)|
|:---:|

# Les vérifieurs

Les vérifieurs sont des fichiers de test PyTest situés dans `<implem_path>/testconf/verifiers/tests`

## Avant de commencer, quelques concepts :

- [[ https://docs.pytest.org/ ]]
- [[ http://sametmax.com/un-gros-guide-bien-gras-sur-les-tests-unitaires-en-python-partie-3/ ]]
- [[ https://openclassrooms.com/fr/courses/4425126-testez-votre-projet-avec-python/4435144-ajoutez-des-tests-avec-pytest ]]

## Règles d'or à connaître avant d'écrire un nouveau vérifieur :

- Le nom d'un vérifieur (le nom du fichier) doit permettre de comprendre ce qu'il fait
    - `check_<product_name>_consistency.py` : indique qu'il s'agit d'un vérifieur contextuel
    - `is_test_<nom_action>_<nom_fonctionnalité>_<product_name>.py` : indique qu'il s'agit d'un vérifieur d'état
- Rendre générique le vérifieur lorsque c'est possible : Vérification des règles communes (grains, dossier, logs, etc..)
    - Utilisables pour d'autres cas de tests
    - Utilisables pour d'autres formulas
    - Utilisables pour d'autres projets
    - etc..
- Créer ses propres fixtures lorsque cela est possible pour
    - Éviter la duplication de code
    - Optimiser les performances
    - etc..

## Types de vérifieurs

**Vérifieurs contextuel**
- S'adapte au contexte : peut être joué à tout moment
- Tombe KO uniquement si la machine est dans un état incohérent, exemples :
    - Certains des rôles présents sur la machines ne correspondent pas à des répertoire d'instance (sauf cas particulier à gérer)
    - La grains des compagnons filebeat correspond à un rôle présent sur la machine, les répertoires d’instance, le service sont présents et dans un état cohérent.

-> Règles de nommage du fichier `check_<produit>_consistency.py`

**Vérifieurs de vérification d'état**
- A n'appeler pour tester un état souhaité, exemples :
    - le `role_cible` n'est pas déployé sur la machine
    - le `role_cible` dispose de la fonctionnalité <nom_fonctionnalité>, son état est <état_requis>
    - la `fonctionnalitée` est attendue sur la machine

-> Règle de nommage du fichier : `is_<produit>_<fonctionnalite>_<actif/absent>.py`
