|[Home](home.md)|
|:---:|

# Les tests cases

Les tests cases sont à décrire dans fichiers yaml, ils doivent être positionné dans `<implem_path>/testconf/testscases`

## Structure d'un test case
Ce fichier décrit un test case
```yaml
# Liste des tags à rattacher au test case
# L'objectif est de pouvoir lier un test à une exigence ou une US, issue, etc..
tags:
  - Ex#Tototata
  - US#345_fonctionnalite_bidule
  - truc_qui_peut_aider_a_comprendre_ce_que_le_test_fait

# List de pillars à charger
# Ces pillars doivent être spécifique au test case
# Les pillars communs doivent être ajoutés dans <implem_path>/testconf/communs
# La liste est par la suite transformée en un pillar en fusionnant successivement ses éléments par surcharge de dictionnaire
# @required
# @type list of dict
pillar_list:
  - include: # `include` est est mot clé permettant d'aller chercher un fichier
    # L'identifiant d'un fichier pillar correspond a son chemin en partant de la racine des tests cases
    - lemonldap.lemonldap-19.indus-10.pillars.pillar_state_simple.DEV.pillar.admin_0.llngll_i0
  - lemonldap_applications:
      dev:
        admin_0:
          llngll_i0:
            lemonldap_rpm_repo:
              repository: ext-snapshot-local
              version: 1.9.20-SNAPSHOT

# Liste de scénarios à jouer dans l'ordre
# @required
# @type: list of dict
steps:
  -
    # Identifiant du scenario à lancer
    # L'identifiant correspond au chemin vers le fichier scenario et à la fonction à lancer
    # @required
    # @type string
    name: exemple_salt.deploiement.install

    # Titre du scenario
    # @optional
    # @default  valeur de identifiant du scenario à lancer
    # @type string
    title: 1 - Deploiement d'un apache lb configure par mine KO car 0 <= 1 mines attendues

    # description du scenario
    # @optional
    # @default "Pas de description"
    # @type string
    description: |
      Suite a la correction de l'issue 677, le deploiement tombe KO si aucune mine n'est retournee lors de la recherche des « end_points ».
      Ce scenario doit donc tomber KO.

    # Resultat attendu du déploiement
    # @optional
    # @default True
    # @type bool
    expected_result: False

    # List des paramètres à passer au scénario
    # @required
    # @type dict
    args:
      # les paramètres sont décrits au niveau du code du vérifieur
      saltenv: 'dev'
      role_cible: 'dev/admin_0/lemonldap-1.9_indus-1.0/llngll_i0/n0'
      pillar: {} # pillar oneshot

    # Une fois le scénario joué, une liste d'identifiant de vérifieur doit être ajoutée pour contrôler la conformité de l'état atteint
    # L'identifiant d'un vérifieur correspond au chemin vers le fichier scenario et à la fonction à lancer
    # @required
    # @type list of string
    Verifiers:
      - exemple_salt.common.install.common_install
      - exemple_salt.common.install.filebeat_compagnon_install
      - exemple_salt.common.install.centreon_grains_presents
      - exemple_salt.lemonldap.indus11.install.lemonldap_common_install
```
