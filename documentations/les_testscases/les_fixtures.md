|[Home](home.md)|
|:---:|

# Les fixtures

Les vérifieurs s'appuient sur différentes fixtures :

## Fixtures par défaut

- [testinfra](https://testinfra.readthedocs.io/en/latest/modules.html)
- saltstack :
  - `salt_minion` : Permet de récupérer un salt-minion ('eq commande salt-call')
  <br />

  - `salt_grains` : Retourne l'ensemble des grains du minion
  <br />

  - `salt_pillars` : Contournement pour accéder aux pillars.
    L'acces aux pillars n'est en effet pas possible via le salt_minion python
    <br />

  - `salt_caller` : Permet de récupérer un client salt caller permettant d'exécuter la plupart des modules.
    A utiliser en cas d'erreur avec le salt_minion. Notamment KeyError: 'master_uri'.
    Voir https://docs.saltstack.com/en/latest/ref/clients/index.html#salt-s-loader-interface
- context_fixtures :
  - `scenario_context` : Permet de récupérer le contexte du scenario (nom, arguments)
  - `role_cible` : Rôle passé en paramètre au scénario
  - `time_context` : Permet de récupérer le datetime pre (`start_time`) et post (`end_time`) déploiement

## Fixtures personnalisées

Il peut être nécessaire de créer des fixtures pour le besoin des tests de vérifiers.
Pour cela, il faut les positionner dans :
  - Les fixtures liées a un projet : `<implem_path>/testconf/verifiers/fixtures`
  - Les fixtures générique framework : `devopstestor/testconf/verifiers/fixtures`    
