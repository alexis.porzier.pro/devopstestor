|[Home](home.md)|
|:---:|

# Les scénarios

Les scénarios sont des fonctions python situés dans `<implem_path>/src/scenarios`

## Règles d'or à connaitre avant d'écrire un nouveau scénario :

- Le nom d'un scénario doit permettre à un profil non technique de comprendre son objectif
- Le nom et le comportement d'un scénario doivent être en phase
- Le comportement et chaque paramètre du scénario doit être décrit

## Exemple de scénarios

### Scenario exemple_salt.deploiement.install
```mermaid
sequenceDiagram
  participant o as Ordonnanceur
  participant s as scenarioController
  participant m as minionController
  o ->> s: lancer_scenario(nom_scenario, saltenv, role_cible, pillar={})
  activate s
    s ->> m: grains_append_roles(role_cible)
    s ->> m: state.apply(role_cible, pillar)
    s -->> o:resultat
  deactivate s

```
### Scenario exemple_salt.deploiement.uninstall
```mermaid
sequenceDiagram  
  participant o as Ordonnanceur
  participant s as scenarioController
  participant m as minionController
  o ->> s: lancer_scenario(nom_scenario, saltenv, role_cible)
  activate s
    s ->> m: grains_remove_roles(role_cible)
    s ->> m: state.apply(role_cible)
    s -->> o: resultat
  deactivate s
```
