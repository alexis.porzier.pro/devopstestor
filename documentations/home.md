# Documentation sur tests automatisés

Les tests automatisés permettent de valider les cinématiques de déploiement et de vérifier la conformité des modification apportées sur la machine ciblée.

## Configuration des tests auto

- [Comment lancer les tests automatisés](lancement_des_tests_automatises/home.md)
- [Configuration de l'outil (optionel)](configuration_testauto.md)

## Créer des tests et les lancer et tester

- [Créer des testscases](les_testscases/home.md)

## Fonctionnement du moteur des tests automatisés

- [Description du fonctionnement](description_fonctionnement.md)
- [Les cas d'utilisation](cas_utilisation.md)
