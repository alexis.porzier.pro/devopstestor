|[Home](home.md)|
|:---:|

# Conception nouveaux tests auto

![Composants](./images/composants.jpg)

## Les composants

Le code des tests automatisés est en deux partie :

* Le moteur de test générique : devopstestor
* L'implémentation métier appelée ici `surcouche`

# Les modules
La solution est composée de différents modules :

### MachineController
Les tests automatisés peuvent contrôler des machines via différentes implémentations

  * DockerController : Créé et contrôle des images et conteneurs Docker
  * LocalhostController : contrôle une machine virtuelle locale

D'autres contrôleurs pourront être développés dans le futur

### Provisionner

Un provisionner permet de communiquer avec une machine par l'intermédiaire de presets de commande ou par l'intérimédiaire d'un gestionnaire de configuration (ex saltstack, ansible, puppet..).
Différentes provisionner peuvent être créé.
Actuellement seul minion_provisionner a été implémenté afin de contrôler un salt-minion

### SourceAccessor

Afin de rendre disponible des sources aux machines, plusieurs SourcesAccessor ont été créé :

 * DirlinkSourceAccessor : lien directe entre la source sur l'hote et la machine
 * CopytemplateSourceAccessor : création d'une copie, et remplacement de variables
 * GitSourceAccessor : Gestion via git (non implémenté)

Il appartient par la suite à l'implémentation de MachineController de les prendre en compte, par exemple lors de son initialisation

### TestCaseLoader

Charge les tests case et retourne un iterateur permettant de lancer un cas de test

### Ordonnanceur

Gestion des threads et lanceur des cas de tests

### TestCaseRunner

Lance un cas de test

### Devopstestor

Initialise le moteur
Chargeur d'objets

### Vérifieurs

Scripts pytest de vérification de l'état de la machine

### Reporting

Créateur de rapports de test (text, html, kibana, etc.)

### Scenarios

Ordonnanceur de commandes au provisionner
Les scenarios sont appelés dans les tests cases

## Phase d'initialisation

**Etapes globales :**
- L'ensemble des configurations sont chargées en mémoire (fichiers conf, arguments ligne de commande)
- Les sources sont si besoin copiées dans des dossiers temporaires
- La liste des testcase est chargée en mémoire (recherche de fichier)
- Les Objets Pythons sont chargées en mémoire et sont prêt à être utilisés

```mermaid
sequenceDiagram
  participant User
  participant couche_implem
  participant Devopstestor
  participant ConfigLoader
  participant SourceManager
  participant TestCaseLoader
  participant TestCase
  participant MachinesFactory
  participant TestcaseExecutorFactory
  participant Ordonnanceur

  User ->> couche_implem: main
  couche_implem ->> Devopstestor: init
  Devopstestor ->> Devopstestor: set PythonPath
  Devopstestor ->> ConfigLoader: load_config(conf_dirs)
  activate ConfigLoader
    ConfigLoader ->> ConfigLoader: load_configfiles
    ConfigLoader ->> ConfigLoader: load_argv
    ConfigLoader -->> Devopstestor: config
  deactivate ConfigLoader

  Devopstestor ->> SourceManager: init(config)
  activate SourceManager
    SourceManager ->> SourceManager: init_sources_accessors(config)
  deactivate SourceManager

  Devopstestor ->> TestCaseLoader: init(config)
  activate TestCaseLoader
    TestCaseLoader ->> TestCaseLoader: search_and_load_testcases(config)
    activate TestCaseLoader
    loop for each testcase
      TestCaseLoader ->> TestCase: init(testcaseconfig)
      TestCaseLoader -->> TestCaseLoader: testcase
    end
    deactivate TestCaseLoader
  deactivate TestCaseLoader

  Devopstestor ->> MachinesFactory: init(config, source_manager)
  activate MachinesFactory
    MachinesFactory ->> MachinesFactory: init_machine_controller(config)
  deactivate MachinesFactory

  Devopstestor ->> TestcaseExecutorFactory: init(config)
  activate TestcaseExecutorFactory
  deactivate TestcaseExecutorFactory

  Devopstestor ->> Ordonnanceur: init(config, source_manager, tests_cases_loader, machines_factory, testcase_executor_factory)
```

## Lancement des testscases

L'Ordonnanceur à le rôle de lancer les testscase.
```mermaid
sequenceDiagram
  participant Devopstestor
  participant Ordonnanceur
  participant TestCaseLoader
  participant CampaignReport
  participant ReportRender
  participant TestCase
  participant MachinesFactory
  participant MachinesController
  participant TestcaseExecutorFactory
  participant TestcaseExecutor
  participant MachinesProvisionner


  Devopstestor ->> Ordonnanceur: init(...)
  activate Ordonnanceur
  Ordonnanceur ->> TestCaseLoader: get_test_cases()
  activate TestCaseLoader
  TestCaseLoader -->> Ordonnanceur: testscases
  deactivate TestCaseLoader

  Ordonnanceur ->> CampaignReport: init()
  activate CampaignReport
    CampaignReport -->> Ordonnanceur: global_report
  deactivate CampaignReport

  loop for each testscases
    Ordonnanceur ->> TestCase : run(machines_factory, testcase_executor_factory)
    activate TestCase
      TestCase ->> MachinesFactory: get_builded_machine_provisionner(machine_name)
      activate MachinesFactory
        MachinesFactory ->> MachinesController:init(machine_name)
        activate MachinesController
          MachinesController ->> MachinesController:load_source_accessor()
          activate MachinesController
          deactivate MachinesController
        deactivate MachinesController
        MachinesController -->> MachinesFactory: machine_controller

        MachinesFactory ->> MachinesProvisionner: init(machine_controller)
        activate MachinesProvisionner
          MachinesProvisionner -->> MachinesFactory: provisionner
        deactivate MachinesProvisionner

        MachinesFactory -->> TestCase: machine_provisionner
      deactivate MachinesFactory

      TestCase ->> TestcaseExecutorFactory: create_testcase_executor(name, tags)
      activate TestcaseExecutorFactory
      TestcaseExecutorFactory -->> TestCase: testcase_executor
      deactivate TestcaseExecutorFactory

      loop for each scenario
        TestCase ->> TestcaseExecutor: add_scenario(scenario_name, verifieurs_list, scenario_args)
        activate TestcaseExecutor
        deactivate TestcaseExecutor
      end
      TestCase ->> TestcaseExecutor: run(provisionner)
      activate TestcaseExecutor
      TestcaseExecutor -->> TestCase: report
      deactivate TestcaseExecutor


      TestCase -->> Ordonnanceur: report
    deactivate TestCase
    Ordonnanceur ->> CampaignReport: add_testcase_report(report)
  end
  Ordonnanceur ->> ReportRender: render(global_report)
  deactivate Ordonnanceur
```
