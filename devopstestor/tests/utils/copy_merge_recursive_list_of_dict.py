from devopstestor.src.lib.utils import copy_merge_recursive_list_of_dict

def test_copy_merge_recursive_list_of_dict():
    # Arrange
    d1 =  {
        "toto": "titi",
        "tata": [123, 'alfred', 12.4],
        "tonton":{
            "bonjour": "monsiueyr"
        }
    }
    d2 = {
        "toto": "hello",
        "tata": ["s2", "oo", {
            "aze": "bfs",
            "tutu": ['chapeau', 'pointu']
        }],
        "tonton":{
            "bonjour": "azed"
        },
        "mec": "Jean"
    }
    d3 = {
        "toto": "nop",
        "new": "field"
    }



    expected =  {
        "toto": "nop",
        "tata": [123, 'alfred', 12.4, 's2', 'oo', {'aze': 'bfs', 'tutu': ['chapeau', 'pointu']}],
        "tonton":{
            "bonjour": "azed"
        },
        "mec": "Jean",
        "new": "field"
    }

    # Verify
    res = copy_merge_recursive_list_of_dict(d1, d2, d3)
    assert expected == res
