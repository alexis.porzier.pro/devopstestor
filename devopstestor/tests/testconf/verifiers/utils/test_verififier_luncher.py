from devopstestor.testconf.verifiers.utils import verififier_luncher
import json, os

def util_load_json_file(file_path):
    with open(util_transform_rel_path(file_path), 'r') as outfile:
        return json.load(outfile)
    raise Exception("Json file open/parse error")
    return None

def util_transform_rel_path(rpath):
    return os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), rpath))

def test_parse_junit_verifiers_report():
    result = verififier_luncher.parse_junit_verifiers_report(
        verifiers_targets = [
            "logstash.logstash_status_is_green",
            "logstash.logstash_pipeline_messages"
        ],
        file_path= util_transform_rel_path("./ressources/params/junit_report.xml"),
        verifieur_not_found = {}
    )

    # Verify
    expected = util_load_json_file("./ressources/expect/verifier_report.json")

    assert result['verifiers']['logstash.logstash_status_is_green'] == expected['verifiers']['logstash.logstash_status_is_green']
    assert result['verifiers']['logstash.logstash_pipeline_messages'] == expected['verifiers']['logstash.logstash_pipeline_messages']
    assert result['verifiers'].get('cle_testcase_introuvable') == expected['verifiers'].get('cle_testcase_introuvable')
