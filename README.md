# devopstestor

Bienvenue sur le nouveau moteur des tests automatisés.  
Ce moteur est entièrement développé en Python 3.6

# Utilisation

Il est nécessaire de créer une couche d'implémentation (voir sample-testauto-project).  
ou d'utiliser un preset

# Documentation
[Documentation](documentations/home.md)
