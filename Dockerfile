FROM python:3.11-slim
ENV DEVOPSTESTOR_PATH='/usr/local/lib/python3.11/site-packages/devopstestor'
ENV PIP_ROOT_USER_ACTION=ignore

# Install dep
RUN apt-get update \
  && apt-get install -y --no-install-recommends git jq \
  && apt-get purge -y --auto-remove \
  && rm -rf /var/lib/apt/lists/*

COPY . /tmp/devopstestor
RUN python3 -m pip  install --upgrade pip  && \
  python3 -m pip install /tmp/devopstestor yq

# Customise devopstestor installation for docker execution
RUN /bin/bash /tmp/devopstestor/devopstestor/presets/saltstack/build4docker.bash
RUN /bin/bash /tmp/devopstestor/devopstestor/presets/logstash/build4docker.bash

# fix bug docker/requests
RUN python3 -m pip install -U --break-system-packages requests==2.31.0

CMD [""]
ENTRYPOINT [ "/usr/local/bin/devopstestor-presets" ]
